#ifndef CMSJMECalculators_JMESystematicsCalculators_H
#define CMSJMECalculators_JMESystematicsCalculators_H

#include <map>
#include <ROOT/RVec.hxx>
#include <variant>

#include <correction.h>

namespace rdfhelpers {

class ModifiedPtMCollection { // map of variation collections
public:
  using compv_t = ROOT::VecOps::RVec<float>;

  ModifiedPtMCollection() = default;
  ModifiedPtMCollection(std::size_t n, const compv_t& pt, const compv_t& mass)
    : m_pt(n, pt), m_mass(n, mass) {}

  std::size_t size() const { return m_pt.size(); }

  const compv_t& pt(std::size_t i) const { return m_pt[i]; }
  const compv_t& mass(std::size_t i) const { return m_mass[i]; }

  void set(std::size_t i, const compv_t& pt, const compv_t& mass) {
    m_pt[i] = pt;
    m_mass[i] = mass;
  }
  void set(std::size_t i, compv_t&& pt, compv_t&& mass) {
    m_pt[i] = std::move(pt);
    m_mass[i] = std::move(mass);
  }
private:
  std::vector<compv_t> m_pt;
  std::vector<compv_t> m_mass;
};


class ModifiedPtMMsdCollection { // for fat jets
public:
  using compv_t = ROOT::VecOps::RVec<float>;

  ModifiedPtMMsdCollection() = default;
  ModifiedPtMMsdCollection(std::size_t n, const compv_t& pt, const compv_t& mass, const compv_t& msd)
    : m_pt(n, pt), m_mass(n, mass), m_msd(n, msd) {}

  std::size_t size() const { return m_pt.size(); }
  std::size_t sizeM() const { return m_mass.size(); }

  const compv_t& pt(std::size_t i) const { return m_pt[i]; }
  const compv_t& mass(std::size_t i) const { return m_mass[i]; }
  const compv_t& msoftdrop(std::size_t i) const { return m_msd[i]; }

  void set(std::size_t i, const compv_t& pt, const compv_t& mass, const compv_t& msd) {
    m_pt[i] = pt;
    m_mass[i] = mass;
    m_msd[i] = msd;
  }

  void set(std::size_t i, compv_t&& pt, compv_t&& mass, compv_t&& msd) {
    m_pt[i] = std::move(pt);
    m_mass[i] = std::move(mass);
    m_msd[i] = std::move(msd);
  }

private:
  std::vector<compv_t> m_pt;
  std::vector<compv_t> m_mass;
  std::vector<compv_t> m_msd;
};


class ModifiedMET {
public:
  using compv_t = ROOT::VecOps::RVec<double>;

  ModifiedMET() = default;
  // initialize with the nominal value for all variations
  ModifiedMET(std::size_t n, double px_nom, double py_nom)
    : m_px(n, px_nom), m_py(n, py_nom) {}

  std::size_t size() const { return m_px.size(); }
  const compv_t& px() const { return m_px; }
  const compv_t& py() const { return m_py; }
  double px (std::size_t i) const { return m_px[i]; }
  double py (std::size_t i) const { return m_py[i]; }
  double pt (std::size_t i) const { return std::sqrt(m_px[i]*m_px[i]+m_py[i]*m_py[i]); }
  double phi(std::size_t i) const { return std::atan2(m_py[i], m_px[i]); }

  void setXY(std::size_t i, double dpx, double dpy) {
    m_px[i] = dpx;
    m_py[i] = dpy;
  }
  void addR_proj(std::size_t i, double cosphi, double sinphi, double dp) {
    m_px[i] += dp*cosphi;
    m_py[i] += dp*sinphi;
  }
private:
  compv_t m_px;
  compv_t m_py;
};
}

class JetMETVariationsCalculatorBase {
public:
  using p4compv_t = ROOT::VecOps::RVec<float>;

  JetMETVariationsCalculatorBase() = default;

  // set up smearing (and JER systematics)
  void setSmearing(
          const correction::Correction::Ref&& jetPtRes,
          const correction::Correction::Ref&& jetEResSF,
          const correction::Correction::Ref&& jerSmear,
          bool splitJER,
          bool doGenMatch,
          float genMatch_maxDR=-1.,
          float genMatch_maxDPT=-1.)
  {
    m_doSmearing = true;
    m_jetPtRes = std::move(jetPtRes);
    m_jetEResSF = std::move(jetEResSF);
    m_jerSmear = std::move(jerSmear);
    m_splitJER = splitJER;
    m_smearDoGenMatch = doGenMatch;
    m_genMatch_dR2max = genMatch_maxDR*genMatch_maxDR;
    m_genMatch_dPtmax = genMatch_maxDPT;
    m_jersfInputEtaPt = (jetEResSF->inputs().size() == 3); // jetEta, jetPt, syst
  }

  void setJEC(const std::variant<correction::Correction::Ref, correction::CompoundCorrection::Ref>&& jesSF) {
    m_doJEC = true;
    if (auto corrObj = std::get_if<correction::Correction::Ref>(&jesSF)) {
      m_jecInputAreaRho = ((*corrObj)->inputs().size() == 4);
      m_jecInputAreaRhoPhi = ((*corrObj)->inputs().size() == 5);
    } else {
      m_jecInputAreaRho = (std::get<correction::CompoundCorrection::Ref>(jesSF)->inputs().size() == 4);
      m_jecInputAreaRhoPhi = (std::get<correction::CompoundCorrection::Ref>(jesSF)->inputs().size() == 5);
    }
    m_jesSF = std::move(jesSF);
  }
  void setAddHEM2018Issue(bool enable) { m_addHEM2018Issue = enable; }

  void addJESUncertainty(const std::string& name, const correction::Correction::Ref&& params)
  {
    m_jesUncSources.emplace(std::piecewise_construct,
        std::forward_as_tuple(name),
        std::forward_as_tuple(params));
  }
protected:
  std::size_t findGenMatch(
    const double pt, const float eta, const float phi, const std::size_t genJetIdx,
    const ROOT::VecOps::RVec<float>& gen_pt, const ROOT::VecOps::RVec<float>& gen_eta,
    const ROOT::VecOps::RVec<float>& gen_phi, const double resolution 
    ) const;

  // config options
  bool m_doSmearing{false}, m_smearDoGenMatch;      // default: yes, yes
  bool m_addHEM2018Issue{false}, m_splitJER{false}; // default: no, no
  float m_genMatch_dR2max, m_genMatch_dPtmax;       // default: R/2 (0.2) and 3
  // parameters and helpers
  correction::Correction::Ref m_jetPtRes;
  correction::Correction::Ref m_jetEResSF;
  correction::Correction::Ref m_jerSmear;
  bool m_doJEC{false}, m_jecInputAreaRho{false}, m_jecInputAreaRhoPhi{false}, m_jersfInputEtaPt{false};
  std::variant<correction::Correction::Ref, correction::CompoundCorrection::Ref> m_jesSF;
  std::map<std::string, correction::Correction::Ref> m_jesUncSources;
};

class JetVariationsCalculator : public JetMETVariationsCalculatorBase {
public:
  using result_t = rdfhelpers::ModifiedPtMCollection;

  JetVariationsCalculator() = default;

  static JetVariationsCalculator create(
      const std::string& jsonFile,
      const std::string& jetAlgo,
      const std::string& jecTag,
      const std::string& jecLevel,
      const std::vector<std::string>& jesUncertainties,
      bool addHEM2018Issue,
      const std::string& jerTag, const std::string& jsonFileSmearingTool,
      const std::string& smearingToolName, bool splitJER,
      bool doGenMatch, float genMatch_maxDR, float genMatch_maxDPT);

  std::vector<std::string> available(const std::string& attr = {}) const;
  // interface for NanoAOD
  result_t produce(
      const p4compv_t& jet_pt, const p4compv_t& jet_eta, const p4compv_t& jet_phi, const p4compv_t& jet_mass,
      const p4compv_t& jet_rawcorr, const p4compv_t& jet_area, const ROOT::VecOps::RVec<int>& jet_jetId,
      const float rho,
      // MC-only
      const ROOT::VecOps::RVec<int>& jet_genJetIdx,
      const ROOT::VecOps::RVec<int>& jet_partonFlavour,
      const int seed,
      const p4compv_t& genjet_pt, const p4compv_t& genjet_eta, const p4compv_t& genjet_phi, const p4compv_t& genjet_mass
      ) const;
};


class Type1METVariationsCalculator : public JetMETVariationsCalculatorBase {
public:
  using result_t = rdfhelpers::ModifiedMET;

  Type1METVariationsCalculator() = default;

 static Type1METVariationsCalculator create(
     const std::string& jsonFile,
     const std::string& jetAlgo,
     const std::string& jecTag,
     const std::string& jecLevel,
     const std::string& l1JecTag,
     float unclEnThreshold,
     float emEnFracThreshold,
     const std::vector<std::string>& jesUncertainties,
     bool addHEM2018Issue,
     bool isT1SmearedMET,
     const std::string& jerTag, const std::string& jsonFileSmearingTool,
     const std::string& smearingToolName, bool splitJER,
     bool doGenMatch, float genMatch_maxDR, float genMatch_maxDPT);

 // additional settings: L1-only JEC
 void setUnclusteredEnergyTreshold(float threshold) { m_unclEnThreshold = threshold; }
 void setEmEnergyFracThreshold(float EMthreshold) {m_emEnFracThreshold = EMthreshold; }
 void setIsT1SmearedMET(bool isT1SmearedMET) { m_isT1SmearedMET = isT1SmearedMET; }
 void setL1JEC(const correction::Correction::Ref&& l1JecTag)
 {
    m_doL1JEC = true;
    m_jetLevel1 = std::move(l1JecTag);
  }

 std::vector<std::string> available(const std::string& attr = {}) const;
 // interface for NanoAOD
 result_t produce(
      const p4compv_t& jet_pt, const p4compv_t& jet_eta, const p4compv_t& jet_phi, const p4compv_t& jet_mass,
      const p4compv_t& jet_rawcorr, const p4compv_t& jet_area, const p4compv_t& jet_muonSubtrFactor, 
      const p4compv_t& jet_neEmEF, const p4compv_t& jet_chEmEF, const ROOT::VecOps::RVec<int>& jet_jetId,
      const float rho,
      // MC-only
      const ROOT::VecOps::RVec<int>& jet_genJetIdx,
      const ROOT::VecOps::RVec<int>& jet_partonFlavour,
      const int seed,
      const p4compv_t& genjet_pt, const p4compv_t& genjet_eta, const p4compv_t& genjet_phi, const p4compv_t& genjet_mass,
     // MET-specific
     const float rawmet_phi, const float rawmet_pt,
     const p4compv_t& lowptjet_rawpt, const p4compv_t& lowptjet_eta, const p4compv_t& lowptjet_phi, const p4compv_t& lowptjet_area,
     const p4compv_t& lowptjet_muonSubtrFactor, const p4compv_t& lowptjet_neEmEF, const p4compv_t& lowptjet_chEmEF,
     const float met_unclustenupdx, const float met_unclustenupdy
     ) const;
protected:
 float m_unclEnThreshold = 15.;
 float m_emEnFracThreshold = 0.9;
 bool m_isT1SmearedMET = false;
 bool m_doL1JEC{false};
 correction::Correction::Ref m_jetLevel1;
 void addVariations(Type1METVariationsCalculator::result_t& out,
      const p4compv_t& jet_pt, const p4compv_t& jet_eta, const p4compv_t& jet_phi, const p4compv_t& jet_mass,
      const p4compv_t& jet_rawcorr, const p4compv_t& jet_area, const p4compv_t& jet_muonSubtrFactor,
      const p4compv_t& jet_neEmEF, const p4compv_t& jet_chEmEF,
      const ROOT::VecOps::RVec<int>& jet_jetId,
      const float rho,
      const ROOT::VecOps::RVec<int>& jet_genJetIdx,
      const ROOT::VecOps::RVec<int>& jet_partonFlavour,
      const p4compv_t& genjet_pt, const p4compv_t& genjet_eta, const p4compv_t& genjet_phi, const p4compv_t& genjet_mass,
		  const int seed) const;

};


class FatJetVariationsCalculator : public JetMETVariationsCalculatorBase {
public:
  using result_t = rdfhelpers::ModifiedPtMMsdCollection;

  FatJetVariationsCalculator() = default;

  static FatJetVariationsCalculator create(
      const std::string& jsonFile,
      const std::string& jetAlgo,
      const std::string& jecTag,
      const std::string& jecLevel,
      const std::vector<std::string>& jesUncertainties,
      bool addHEM2018Issue,
      const std::string& jerTag, const std::string& jsonFileSmearingTool,
      const std::string& smearingToolName, bool splitJER,
      bool doGenMatch, float genMatch_maxDR, float genMatch_maxDPT,
      const std::string& jsonFileSubjet,
      const std::string& jetAlgoSubjet,
      const std::string& jecTagSubjet,
      const std::string& jecLevelSubjet);

  std::vector<std::string> available(const std::string& attr = {}) const;
  // interface for NanoAOD
  result_t produce(
      const p4compv_t& jet_pt, const p4compv_t& jet_eta, const p4compv_t& jet_phi, const p4compv_t& jet_mass,
      const p4compv_t& jet_rawcorr, const p4compv_t& jet_area,
      const p4compv_t& jet_msoftdrop, const ROOT::VecOps::RVec<int>& jet_subJetIdx1, const ROOT::VecOps::RVec<int>& jet_subJetIdx2,
      const p4compv_t& subjet_pt, const p4compv_t& subjet_eta, const p4compv_t& subjet_phi, const p4compv_t& subjet_mass, const p4compv_t& subjet_rawFactor,
      const ROOT::VecOps::RVec<int>& jet_jetId, const float rho,
      // MC-only
      const ROOT::VecOps::RVec<int>& jet_genJetIdx,
      const int seed,
      const p4compv_t& genjet_pt, const p4compv_t& genjet_eta, const p4compv_t& genjet_phi, const p4compv_t& genjet_mass
      ) const;

  void addJESUncertaintySubjet(const std::string& name, const correction::Correction::Ref&& params)
  {
    m_jesUncSourcesSubjet.emplace(std::piecewise_construct,
        std::forward_as_tuple(name),
        std::forward_as_tuple(params));
  }
  void setJECSubjet(const std::variant<correction::Correction::Ref, correction::CompoundCorrection::Ref>&& jesSF) {
    m_doJECSubjet = true;
    if (auto corrObj = std::get_if<correction::Correction::Ref>(&jesSF)) {
      m_jecInputAreaRhoSubjet = ((*corrObj)->inputs().size() == 4);
      m_jecInputAreaRhoPhiSubjet = ((*corrObj)->inputs().size() == 5);
    } else {
      m_jecInputAreaRhoSubjet = (std::get<correction::CompoundCorrection::Ref>(jesSF)->inputs().size() == 4);
      m_jecInputAreaRhoPhiSubjet = (std::get<correction::CompoundCorrection::Ref>(jesSF)->inputs().size() == 5);
    }
    m_jesSFSubjet = std::move(jesSF);
  }

protected:
  std::variant<correction::Correction::Ref, correction::CompoundCorrection::Ref> m_jesSFSubjet;
  std::map<std::string, correction::Correction::Ref> m_jesUncSourcesSubjet;
  bool m_jecInputAreaRhoSubjet{false}, m_jecInputAreaRhoPhiSubjet{false}, m_doJECSubjet{false};
};

#endif // CMSJMECalculators_JMESystematicsCalculators_H
