#include "JMESystematicsCalculators.h"

#include <Math/GenVector/LorentzVector.h>
#include <Math/GenVector/PtEtaPhiM4D.h>
#include "Math/VectorUtil.h"

#include <cassert>
#include <algorithm>

#include <cstdint>
#include <vector>
#include <variant>
#include <string>

//#define BAMBOO_JME_DEBUG // uncomment to debug

#ifdef BAMBOO_JME_DEBUG
#define LogDebug std::cout
#else
#define LogDebug if (false) std::cout
#endif

using namespace correction;

namespace {
  // because something goes wrong with linking ROOT::Math::VectorUtil::Phi_mpi_pi
  template<typename T>
  T phi_mpi_pi(T angle) {
    if ( angle <= M_PI && angle > -M_PI ) {
      return angle;
    }
    if ( angle > 0 ) {
      const int n = static_cast<int>(.5*(angle*M_1_PI+1.));
      angle -= 2*n*M_PI;
    } else {
      const int n = static_cast<int>(-.5*(angle*M_1_PI-1.));
      angle += 2*n*M_PI;
    }
    return angle;
  }

  // Temporary recipe to evaluate the HEM issue uncertainty in 2018
  float deltaHEM2018Issue(float pt_nom, int jetId, float phi, float eta ) {
    float delta = 1.;
    if ( pt_nom > 15. && ( jetId & 0x2 ) && phi > -1.57 && phi < -0.87 ) {
      if ( eta > -2.5 && eta < -1.3 ) {
        delta = 0.8;
      } else if ( eta <= -2.5 && eta > -3. ) {
        delta = 0.65;
      }
    }
    return delta;
  }

  int jerSplitID(float pt, float eta) {
    const auto aEta = std::abs(eta);
    if ( aEta < 1.93 )
      return 0;
    else if ( aEta < 2.5 )
      return 1;
    else if ( aEta < 3. )
      if ( pt < 50. )
        return 2;
      else
        return 3;
    else
      if ( pt < 50. )
        return 4;
      else
        return 5;
  }

  template<typename CALC>
  void configureBaseCalc(CALC& calc,
    const std::unique_ptr<CorrectionSet>& cset,
    const std::unique_ptr<CorrectionSet>& csetJerSmear,
    const std::string& jetAlgo,
    const std::string& jecTag,
    const std::string& jecLevel,
    const std::vector<std::string>& jesUncertainties,
    const std::string& jerTag, bool splitJER,
    const std::string& smearingToolName,
    bool doGenMatch, float genMatch_maxDR, float genMatch_maxDPT)
  {
    if (!jecLevel.empty()) {
      const std::string key = jecTag + "_" + jecLevel + "_" + jetAlgo;
      auto maybe_corr = std::find_if(cset->begin(), cset->end(),
            [key](const auto& elem){ return elem.first == key; } );
      if (maybe_corr != cset->end()) {
        calc.setJEC(std::move(cset->at(key)));
      } else {
        calc.setJEC(std::move(cset->compound().at(key)));
      }
    }
    for (const auto& unc: jesUncertainties) {
      const std::string key = jecTag + "_" + unc + "_" + jetAlgo;
      calc.addJESUncertainty(unc, std::move(cset->at(key)));
    }
    if (!jerTag.empty()) {
      const std::string resKey = jerTag + "_PtResolution_" + jetAlgo;
      const std::string sfKey = jerTag + "_ScaleFactor_" + jetAlgo;
      const std::string jsKey = smearingToolName;
      calc.setSmearing(std::move(cset->at(resKey)), std::move(cset->at(sfKey)), std::move(csetJerSmear->at(jsKey)),
              splitJER, doGenMatch, genMatch_maxDR, genMatch_maxDPT);
    }
  }

  template<typename CALC>
  void configureFatjetCalc(CALC& calc,
    const std::unique_ptr<CorrectionSet>& cset,
    const std::unique_ptr<CorrectionSet>& csetJerSmear,
    const std::string& jetAlgo,
    const std::string& jecTag,
    const std::string& jecLevel,
    const std::vector<std::string>& jesUncertainties,
    const std::string& jerTag, bool splitJER,
    const std::string& smearingToolName,
    bool doGenMatch, float genMatch_maxDR, float genMatch_maxDPT,
    const std::unique_ptr<CorrectionSet>& csetSubjet,
    const std::string& jetAlgoSubjet,
    const std::string& jecTagSubjet,
    const std::string& jecLevelSubjet)
  {
    if (!jecLevel.empty()) {
      const std::string key = jecTag + "_" + jecLevel + "_" + jetAlgo;
      auto maybe_corr = std::find_if(cset->begin(), cset->end(),
            [key](const auto& elem){ return elem.first == key; } );
      if (maybe_corr != cset->end()) {
        calc.setJEC(std::move(cset->at(key)));
      } else {
        calc.setJEC(std::move(cset->compound().at(key)));
      }
    }
    for (const auto& unc: jesUncertainties) {
      const std::string key = jecTag + "_" + unc + "_" + jetAlgo;
      calc.addJESUncertainty(unc, std::move(cset->at(key)));
    }
    if (!jerTag.empty()) {
      const std::string resKey = jerTag + "_PtResolution_" + jetAlgo;
      const std::string sfKey = jerTag + "_ScaleFactor_" + jetAlgo;
      const std::string jsKey = smearingToolName;
      calc.setSmearing(std::move(cset->at(resKey)), std::move(cset->at(sfKey)), std::move(csetJerSmear->at(jsKey)),
              splitJER, doGenMatch, genMatch_maxDR, genMatch_maxDPT);
    }
    // subjets
    if (!jecLevelSubjet.empty()) {
      const std::string key = jecTagSubjet + "_" + jecLevelSubjet + "_" + jetAlgoSubjet;
      auto maybe_corr = std::find_if(csetSubjet->begin(), csetSubjet->end(),
            [key](const auto& elem){ return elem.first == key; } );
      if (maybe_corr != csetSubjet->end()) {
        calc.setJECSubjet(std::move(csetSubjet->at(key)));
      } else {
        calc.setJECSubjet(std::move(csetSubjet->compound().at(key)));
      }
      for (const auto& unc: jesUncertainties) {
        const std::string key = jecTagSubjet + "_" + unc + "_" + jetAlgoSubjet;
        calc.addJESUncertaintySubjet(unc, std::move(csetSubjet->at(key)));
      }
    }
  }

  template<typename CALC>
  void configureMETCalc_common(CALC& calc,
      const std::unique_ptr<CorrectionSet>& cset,
      const std::unique_ptr<CorrectionSet>& csetJerSmear,
      const std::string& jetAlgo,
      const std::string& jecTag,
      const std::string& jecLevel,
      const std::string& l1jec,
      float unclEnThreshold,
      float emEnFracThreshold,
      const std::vector<std::string>& jesUncertainties,
      bool isT1SmearedMET,
      const std::string& jerTag, bool splitJER,
      const std::string& smearingToolName,
      bool doGenMatch, float genMatch_maxDR, float genMatch_maxDPT)
    {
      if (!jecLevel.empty()) {
        const std::string key = jecTag + "_" + jecLevel + "_" + jetAlgo;
        auto maybe_corr = std::find_if(cset->begin(), cset->end(),
            [key](const auto& elem){ return elem.first == key; } );
        if (maybe_corr != cset->end()) {
          calc.setJEC(std::move(cset->at(key)));
        } else {
          calc.setJEC(std::move(cset->compound().at(key)));
        }
      }
      for (const auto& unc: jesUncertainties) {
        const std::string key = jecTag + "_" + unc + "_" + jetAlgo;
        calc.addJESUncertainty(unc, std::move(cset->at(key)));
      }
      if (!jerTag.empty()) {
        const std::string resKey = jerTag + "_PtResolution_" + jetAlgo;
        const std::string sfKey = jerTag + "_ScaleFactor_" + jetAlgo;
        const std::string jsKey = smearingToolName;
        calc.setSmearing(std::move(cset->at(resKey)), std::move(cset->at(sfKey)), std::move(csetJerSmear->at(jsKey)),
                splitJER, doGenMatch, genMatch_maxDR, genMatch_maxDPT);
      }
      if (!l1jec.empty()){
        const std::string l1Key = jecTag + "_" + l1jec + "_" + jetAlgo;
        calc.setL1JEC(std::move(cset->at(l1Key)));
      }
    }
}

// TODO with orig MET and jets (sumpx,sumpy): calc modif MET(sig), produce bigger results type

std::size_t JetMETVariationsCalculatorBase::findGenMatch(const double pt, const float eta, const float phi, const std::size_t genJetIdx, const ROOT::VecOps::RVec<float>& gen_pt, const ROOT::VecOps::RVec<float>& gen_eta, const ROOT::VecOps::RVec<float>& gen_phi, const double resolution ) const
{
  auto get_dr2 = [](float phi, float eta, float gen_phi, float gen_eta) -> float {
    const auto dphi = phi_mpi_pi(gen_phi - phi);
    const auto deta = gen_eta - eta;
    return dphi*dphi + deta*deta;
  };
  auto check_resolution = [this, resolution](float pt, float gen_pt) -> bool {
    return std::abs(gen_pt - pt) < m_genMatch_dPtmax*resolution;
  };

  // First check if matched genJet from NanoAOD is acceptable
  if (genJetIdx >= 0) {
      const float dr2 = get_dr2(phi, eta, gen_phi[genJetIdx], gen_eta[genJetIdx]);
      if ((dr2 < m_genMatch_dR2max) && check_resolution(pt, gen_pt[genJetIdx])) {
        LogDebug << "Using matched genJet from NanoAOD, dr2=" << dr2 << std::endl;
        return genJetIdx;
      }
  }

  std::size_t igBest{gen_pt.size()};
  auto dr2Min = std::numeric_limits<float>::max();
  LogDebug << "(DRs: ";
  for ( std::size_t ig{0}; ig != gen_pt.size(); ++ig ) {
    const auto dr2 = get_dr2(phi, eta, gen_phi[ig], gen_eta[ig]);
    LogDebug << "dr2=" << dr2;
    if ( ( dr2 < dr2Min ) && ( dr2 < m_genMatch_dR2max ) ) {
      LogDebug << "->dpt=" << std::abs(gen_pt[ig]-pt) << ",res=" << resolution;
      if (check_resolution(pt, gen_pt[ig])) {
        LogDebug << "->best:" << ig;
        dr2Min = dr2;
        igBest = ig;
      }
    }
    LogDebug << ", ";
  }
  LogDebug << ")";
  return igBest;
}

JetVariationsCalculator JetVariationsCalculator::create(
    const std::string& jsonFile,
    const std::string& jetAlgo,
    const std::string& jecTag,
    const std::string& jecLevel,
    const std::vector<std::string>& jesUncertainties,
    bool addHEM2018Issue,
    const std::string& jerTag, const std::string& jsonFileSmearingTool,
    const std::string& smearingToolName, bool splitJER,
    bool doGenMatch, float genMatch_maxDR, float genMatch_maxDPT)
{
  JetVariationsCalculator inst{};
  auto cset = CorrectionSet::from_file(jsonFile);
  std::unique_ptr<correction::CorrectionSet> csetJerSmear = nullptr;
  if(!jsonFileSmearingTool.empty())
    csetJerSmear = CorrectionSet::from_file(jsonFileSmearingTool);
  configureBaseCalc(inst, cset, csetJerSmear, jetAlgo, jecTag, jecLevel, jesUncertainties,
    jerTag, splitJER, smearingToolName, doGenMatch, genMatch_maxDR, genMatch_maxDPT);
  inst.setAddHEM2018Issue(addHEM2018Issue);
  return std::move(inst);
}

JetVariationsCalculator::result_t JetVariationsCalculator::produce(
    const p4compv_t& jet_pt, const p4compv_t& jet_eta, const p4compv_t& jet_phi, const p4compv_t& jet_mass,
    const p4compv_t& jet_rawcorr, const p4compv_t& jet_area,
    const ROOT::VecOps::RVec<int>& jet_jetId,
    const float rho,
    const ROOT::VecOps::RVec<int>& jet_genJetIdx,
    const ROOT::VecOps::RVec<int>& jet_partonFlavour,
    const int seed,
    const p4compv_t& genjet_pt, const p4compv_t& genjet_eta, const p4compv_t& genjet_phi, const p4compv_t& genjet_mass ) const
{
  const auto nVariations = 1+( m_doSmearing ? 2*( m_splitJER ? 6 : 1 ) : 0 )+2*m_jesUncSources.size()+( m_addHEM2018Issue ? 2 : 0 ); // 1(nom)+2(JER)+2*len(JES)[+2(HEM)]
  LogDebug << "JME:: hello from JetVariations produce. Got " << jet_pt.size() << " jets" << std::endl;
  const auto nJets = jet_pt.size();
  result_t out{nVariations, jet_pt, jet_mass};
  ROOT::VecOps::RVec<double> pt_nom{jet_pt}, mass_nom{jet_mass};
  if ( m_doJEC ) {
    LogDebug << "JME:: reapplying JEC" << std::endl;
    for ( std::size_t i{0}; i != nJets; ++i ) {
      float corr;
      if (auto corrObj = std::get_if<Correction::Ref>(&m_jesSF)) {
        if (m_jecInputAreaRho)
          corr = (*corrObj)->evaluate({jet_area[i], jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
        else if (m_jecInputAreaRhoPhi)
           corr = (*corrObj)->evaluate({jet_area[i], jet_eta[i], jet_phi[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
        else
          corr = (*corrObj)->evaluate({jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i])});
      } else {
        if (m_jecInputAreaRho)
          corr = std::get<CompoundCorrection::Ref>(m_jesSF)->evaluate({jet_area[i], jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
        else if (m_jecInputAreaRhoPhi)
          corr = std::get<CompoundCorrection::Ref>(m_jesSF)->evaluate({jet_area[i], jet_eta[i], jet_phi[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
        else
          corr = std::get<CompoundCorrection::Ref>(m_jesSF)->evaluate({jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i])});
      }
      if ( corr > 0. ) {
        const double newc = (1.-jet_rawcorr[i])*corr;
        pt_nom[i]   *= newc;
        mass_nom[i] *= newc;
      }
    }
#ifdef BAMBOO_JME_DEBUG
    LogDebug << "JME:: with reapplied JEC: ";
    for ( std::size_t i{0}; i != nJets; ++i ) {
      LogDebug << "(PT=" << pt_nom[i] << ", ETA=" << jet_eta[i] << ", PHI=" << jet_phi[i] << ", M=" << mass_nom[i] << ") ";
    }
    LogDebug << std::endl;
#endif
  } else {
    LogDebug << "JME:: Not reapplying JEC" << std::endl;
  }
  // smearing and JER
  std::size_t iVar = 1; // after nominal
  if ( m_doSmearing ) {
    LogDebug << "JME:: Smearing (seed=" << seed << ")" << std::endl;
    p4compv_t pt_jerUp(pt_nom.size(), 0.), mass_jerUp(mass_nom.size(), 0.);
    p4compv_t pt_jerDown(pt_nom.size(), 0.), mass_jerDown(mass_nom.size(), 0.);
    for ( std::size_t i{0}; i != nJets; ++i ) {
      const auto eOrig = ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>>(pt_nom[i], jet_eta[i], jet_phi[i], mass_nom[i]).E();
      double smearFactor_nom{1.}, smearFactor_down{1.}, smearFactor_up{1.};
      //double sfNom{1.}, sfUp{1.}, sfDown{1.};
      if ( pt_nom[i] > 0. ) {
        const auto ptRes  = m_jetPtRes->evaluate({jet_eta[i], pt_nom[i], rho});
        LogDebug << "JME:: JetParameters: pt=" << pt_nom[i] << ", eta=" << jet_eta[i] << ", rho=" << rho << "; ptRes=" << ptRes << std::endl;
        LogDebug << "JME:: ";
        float genPt = -1;
        if ( m_smearDoGenMatch ) {
          const auto iGen = findGenMatch(pt_nom[i], jet_eta[i], jet_phi[i], jet_genJetIdx[i], genjet_pt, genjet_eta, genjet_phi, ptRes*pt_nom[i]);
          if ( iGen != genjet_pt.size() ) {
            genPt = genjet_pt[iGen];
            LogDebug << "genPt=" << genPt << " ";
          }
        }
        if ( m_jersfInputEtaPt ) {
          smearFactor_nom  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], pt_nom[i], "nom"})});
          smearFactor_up  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], pt_nom[i], "up"})});
          smearFactor_down  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], pt_nom[i], "down"})});
        } else {
          smearFactor_nom  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], "nom"})});
          smearFactor_up  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], "up"})});
          smearFactor_down  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], "down"})});
        }
        LogDebug << "JME::  SmearFactors are: NOMINAL=" << smearFactor_nom << ", DOWN=" << smearFactor_down << ", UP=" << smearFactor_up << std::endl;
      }
      pt_jerDown[i]   = pt_nom[i]*smearFactor_down;
      mass_jerDown[i] = mass_nom[i]*smearFactor_down;
      pt_jerUp[i]     = pt_nom[i]*smearFactor_up;
      mass_jerUp[i]   = mass_nom[i]*smearFactor_up;
      pt_nom[i]       *= smearFactor_nom;
      mass_nom[i]     *= smearFactor_nom;
    }
    if ( m_splitJER ) {
      ROOT::VecOps::RVec<int> jerBin(pt_nom.size(), -1);
      for ( std::size_t j{0}; j != nJets; ++j ) {
        jerBin[j] = jerSplitID(pt_nom[j], jet_eta[j]);
      }
      for ( int i{0}; i != 6; ++i ) {
        p4compv_t pt_jeriUp{pt_nom}, mass_jeriUp{mass_nom};
        p4compv_t pt_jeriDown{pt_nom}, mass_jeriDown{mass_nom};
        for ( std::size_t j{0}; j != nJets; ++j ) {
          if ( jerBin[j] == i ) {
            pt_jeriUp[j] = pt_jerUp[j];
            pt_jeriDown[j] = pt_jerDown[j];
            mass_jeriUp[j] = mass_jerUp[j];
            mass_jeriDown[j] = mass_jerDown[j];
          }
        }
        out.set(iVar++, std::move(pt_jeriUp)  , std::move(mass_jeriUp)  );
        out.set(iVar++, std::move(pt_jeriDown), std::move(mass_jeriDown));
      }
    } else {
      out.set(iVar++, std::move(pt_jerUp)  , std::move(mass_jerUp)  );
      out.set(iVar++, std::move(pt_jerDown), std::move(mass_jerDown));
    }
    LogDebug << "JME:: Done with smearing" << std::endl;
  } else {
    LogDebug << "JME:: No smearing" << std::endl;
  }

  // Nominal = first entry in result
  out.set(0, pt_nom, mass_nom);

  // HEM issue 2018, see https://hypernews.cern.ch/HyperNews/CMS/get/JetMET/2000.html
  if ( m_addHEM2018Issue ) {
    p4compv_t pt_down(pt_nom.size(), 0.), mass_down(mass_nom.size(), 0.);
    for ( std::size_t j{0}; j != nJets; ++j ) {
      const auto delta = deltaHEM2018Issue(pt_nom[j], jet_jetId[j], jet_phi[j], jet_eta[j]);
      pt_down[j] = pt_nom[j]*delta;
      mass_down[j] = mass_nom[j]*delta;
    }
    out.set(iVar++, pt_nom, mass_nom);
    out.set(iVar++, std::move(pt_down), std::move(mass_down));
  }
  // JES uncertainties
  for ( auto& jesUnc : m_jesUncSources ) {
    LogDebug << "JME:: evaluating JES uncertainty: " << jesUnc.first << std::endl;
    p4compv_t pt_jesDown(pt_nom.size(), 0.), mass_jesDown(mass_nom.size(), 0.);
    p4compv_t pt_jesUp(pt_nom.size(), 0.), mass_jesUp(mass_nom.size(), 0.);
    for ( std::size_t i{0}; i != nJets; ++i ) {
      float delta = 0.;
      const auto partonFlav = std::abs(jet_partonFlavour[i]);
      if (!(jesUnc.first == "FlavorPureGluon" && partonFlav != 21) &&
          !(jesUnc.first == "FlavorPureQuark" && !(partonFlav >= 1 && partonFlav <= 3)) &&
          !(jesUnc.first == "FlavorPureCharm" && partonFlav != 4) &&
          !(jesUnc.first == "FlavorPureBottom" && partonFlav != 5)) {
          delta = jesUnc.second->evaluate({jet_eta[i], pt_nom[i]});
      }
      LogDebug << "JME:: jet " << i << ", parton flavour = " << partonFlav << ", delta = " << delta << std::endl;
      pt_jesDown[i]   = pt_nom[i]*(1.-delta);
      mass_jesDown[i] = mass_nom[i]*(1.-delta);
      pt_jesUp[i]     = pt_nom[i]*(1.+delta);
      mass_jesUp[i]   = mass_nom[i]*(1.+delta);
    }
    out.set(iVar++, std::move(pt_jesUp)  , std::move(mass_jesUp)  );
    out.set(iVar++, std::move(pt_jesDown), std::move(mass_jesDown));
  }

#ifdef BAMBOO_JME_DEBUG
  assert(iVar == out.size());
  LogDebug << "JME:: returning " << out.size() << " modified jet collections" << std::endl;
  const auto varNames = available();
  assert(varNames.size() == nVariations);
  for ( std::size_t i{0}; i != nVariations; ++i ) {
    LogDebug << "JME:: Jet_" << varNames[i] << ": ";
    for ( std::size_t j{0}; j != nJets; ++j ) {
      LogDebug << "(PT=" << out.pt(i)[j] << ", ETA=" << jet_eta[j] << ", PHI=" << jet_phi[j] << ", M=" << out.mass(i)[j] << ") ";
    }
    LogDebug << std::endl;
  }
#endif
  return out;
}

std::vector<std::string> JetVariationsCalculator::available(const std::string&) const
{
  std::vector<std::string> products = { "nominal" };
  if ( m_doSmearing ) {
    if ( m_splitJER ) {
      for ( int i = 0; i != 6; ++i ) {
        products.emplace_back("jer"+std::to_string(i)+"up");
        products.emplace_back("jer"+std::to_string(i)+"down");
      }
    } else {
      products.emplace_back("jerup");
      products.emplace_back("jerdown");
    }
  }
  if ( m_addHEM2018Issue ) {
    products.emplace_back("jesHEMIssueup");
    products.emplace_back("jesHEMIssuedown");
  }
  for ( const auto& src : m_jesUncSources ) {
    products.emplace_back("jes"+src.first+"up");
    products.emplace_back("jes"+src.first+"down");
  }
  return products;
}


FatJetVariationsCalculator FatJetVariationsCalculator::create(
    const std::string& jsonFile,
    const std::string& jetAlgo,
    const std::string& jecTag,
    const std::string& jecLevel,
    const std::vector<std::string>& jesUncertainties,
    bool addHEM2018Issue,
    const std::string& jerTag, const std::string& jsonFileSmearingTool,
    const std::string& smearingToolName, bool splitJER,
    bool doGenMatch, float genMatch_maxDR, float genMatch_maxDPT,
    const std::string& jsonFileSubjet,
    const std::string& jetAlgoSubjet,
    const std::string& jecTagSubjet,
    const std::string& jecLevelSubjet)
{
  FatJetVariationsCalculator inst{};
  auto cset = CorrectionSet::from_file(jsonFile);

  std::unique_ptr<correction::CorrectionSet> csetJerSmear = nullptr;
  if(!jsonFileSmearingTool.empty())
    csetJerSmear = CorrectionSet::from_file(jsonFileSmearingTool);

  std::unique_ptr<correction::CorrectionSet> csetSubjet = nullptr;
  if(!jsonFileSubjet.empty())
    csetSubjet = CorrectionSet::from_file(jsonFileSubjet);

  configureFatjetCalc(inst, cset, csetJerSmear, jetAlgo, jecTag, jecLevel, jesUncertainties,
    jerTag, splitJER, smearingToolName, doGenMatch, genMatch_maxDR, genMatch_maxDPT,
    csetSubjet, jetAlgoSubjet, jecTagSubjet, jecLevelSubjet);
  inst.setAddHEM2018Issue(addHEM2018Issue);
  return std::move(inst);
}


FatJetVariationsCalculator::result_t FatJetVariationsCalculator::produce(
    const p4compv_t& jet_pt, const p4compv_t& jet_eta, const p4compv_t& jet_phi, const p4compv_t& jet_mass,
    const p4compv_t& jet_rawcorr, const p4compv_t& jet_area, const p4compv_t& jet_msoftdrop, const ROOT::VecOps::RVec<int>& jet_subJetIdx1, const ROOT::VecOps::RVec<int>& jet_subJetIdx2,
    const p4compv_t& subjet_pt, const p4compv_t& subjet_eta, const p4compv_t& subjet_phi, const p4compv_t& subjet_mass, const p4compv_t& subjet_rawFactor,
    const ROOT::VecOps::RVec<int>& jet_jetId, const float rho, const ROOT::VecOps::RVec<int>& jet_genJetIdx,
    const int seed,
    const p4compv_t& genjet_pt, const p4compv_t& genjet_eta, const p4compv_t& genjet_phi, const p4compv_t& genjet_mass) const

{
  const auto nVariations = 1+( m_doSmearing ? 2*( m_splitJER ? 6 : 1 ) : 0 )+2*m_jesUncSources.size()+( m_addHEM2018Issue ? 2 : 0 ); // 1(nom)+2(JER)+2*len(JES)[+2(HEM)]
  LogDebug << "JME:: hello from FatJetVariations produce. Got " << jet_pt.size() << " jets" << std::endl;
  LogDebug << "JME:: variations for PT: " << nVariations << std::endl;
  // Only JEC L1Fastjet depends on area and we dont have subjet_are
  // No problem, since subjets are Puppijets and L1Fastjet is not needed for them 
  p4compv_t subjet_area(subjet_pt.size(), 0.);
  
  const auto nJets = jet_pt.size();
  result_t out{nVariations, jet_pt, jet_mass, jet_msoftdrop};
  ROOT::VecOps::RVec<double> pt_nom{jet_pt}, mass_nom{jet_mass};
  ROOT::VecOps::RVec<double> pt_sj_nom{subjet_pt}, mass_sj_nom{subjet_mass};
  using LVectorM = ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>>;
  auto jet_groomedP4 = std::vector<LVectorM>{nJets};
  ROOT::VecOps::RVec<double> msd_nom(nJets, 0.);

  if ( m_doJEC ) {
    LogDebug << "JME:: reapplying JEC" << std::endl;
    for ( std::size_t i{0}; i != nJets; ++i ) {
      float corr;
      if (auto corrObj = std::get_if<Correction::Ref>(&m_jesSF)) {
        if (m_jecInputAreaRho)
          corr = (*corrObj)->evaluate({jet_area[i], jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
        else if (m_jecInputAreaRhoPhi)
          corr = (*corrObj)->evaluate({jet_area[i], jet_eta[i], jet_phi[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
        else
          corr = (*corrObj)->evaluate({jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i])});
      } else {
        if (m_jecInputAreaRho)
          corr = std::get<CompoundCorrection::Ref>(m_jesSF)->evaluate({jet_area[i], jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
        else if (m_jecInputAreaRhoPhi)
          corr = std::get<CompoundCorrection::Ref>(m_jesSF)->evaluate({jet_area[i], jet_eta[i], jet_phi[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
        else
          corr = std::get<CompoundCorrection::Ref>(m_jesSF)->evaluate({jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i])});
      }
      if ( corr > 0. ) {
        const double newc = (1.-jet_rawcorr[i])*corr;
        pt_nom[i]   *= newc;
        mass_nom[i] *= newc;
      }
    }
#ifdef BAMBOO_JME_DEBUG
    LogDebug << "JME:: with reapplied JEC: ";
    for ( std::size_t i{0}; i != nJets; ++i ) {
      LogDebug << "(PT=" << pt_nom[i] << ", ETA=" << jet_eta[i] << ", PHI=" << jet_phi[i] << ", M=" << mass_nom[i] << ") ";
    }
    LogDebug << std::endl;
#endif
  } else {
    LogDebug << "JME:: Not reapplying JEC" << std::endl;
  }

  if ( m_doJECSubjet ) {
    // calculate groomed P4 (and mass)
    for ( std::size_t j{0}; j != nJets; ++j ) {
      if ( jet_subJetIdx1[j] >= 0 && jet_subJetIdx2[j] >= 0 ) {
        float corrS1, corrS2;
        if (auto corrObj = std::get_if<Correction::Ref>(&m_jesSFSubjet)) {
          if (m_jecInputAreaRhoSubjet){
            corrS1 = (*corrObj)->evaluate({subjet_area[jet_subJetIdx1[j]], subjet_eta[jet_subJetIdx1[j]], subjet_pt[jet_subJetIdx1[j]]*(1.-subjet_rawFactor[jet_subJetIdx1[j]]), rho});
            corrS2 = (*corrObj)->evaluate({subjet_area[jet_subJetIdx2[j]], subjet_eta[jet_subJetIdx2[j]], subjet_pt[jet_subJetIdx2[j]]*(1.-subjet_rawFactor[jet_subJetIdx2[j]]), rho});
          } else if(m_jecInputAreaRhoPhiSubjet){
            corrS1 = (*corrObj)->evaluate({subjet_area[jet_subJetIdx1[j]], subjet_eta[jet_subJetIdx1[j]], subjet_phi[jet_subJetIdx1[j]], subjet_pt[jet_subJetIdx1[j]]*(1.-subjet_rawFactor[jet_subJetIdx1[j]]), rho});
            corrS2 = (*corrObj)->evaluate({subjet_area[jet_subJetIdx2[j]], subjet_eta[jet_subJetIdx2[j]], subjet_phi[jet_subJetIdx2[j]], subjet_pt[jet_subJetIdx2[j]]*(1.-subjet_rawFactor[jet_subJetIdx2[j]]), rho});
          } else {
            corrS1 = (*corrObj)->evaluate({subjet_eta[jet_subJetIdx1[j]], subjet_pt[jet_subJetIdx1[j]]*(1.-subjet_rawFactor[jet_subJetIdx1[j]])});
            corrS2 = (*corrObj)->evaluate({subjet_eta[jet_subJetIdx2[j]], subjet_pt[jet_subJetIdx2[j]]*(1.-subjet_rawFactor[jet_subJetIdx2[j]])});
          }
        } else {
          if (m_jecInputAreaRhoSubjet){
            corrS1 = std::get<CompoundCorrection::Ref>(m_jesSFSubjet)->evaluate({subjet_area[jet_subJetIdx1[j]], subjet_eta[jet_subJetIdx1[j]], subjet_pt[jet_subJetIdx1[j]]*(1.-subjet_rawFactor[jet_subJetIdx1[j]]), rho});
            corrS2 = std::get<CompoundCorrection::Ref>(m_jesSFSubjet)->evaluate({subjet_area[jet_subJetIdx2[j]], subjet_eta[jet_subJetIdx2[j]], subjet_pt[jet_subJetIdx2[j]]*(1.-subjet_rawFactor[jet_subJetIdx2[j]]), rho});
          } else if(m_jecInputAreaRhoPhiSubjet){
            corrS1 = std::get<CompoundCorrection::Ref>(m_jesSFSubjet)->evaluate({subjet_area[jet_subJetIdx1[j]], subjet_eta[jet_subJetIdx1[j]], subjet_phi[jet_subJetIdx1[j]], subjet_pt[jet_subJetIdx1[j]]*(1.-subjet_rawFactor[jet_subJetIdx1[j]]), rho});
            corrS2 = std::get<CompoundCorrection::Ref>(m_jesSFSubjet)->evaluate({subjet_area[jet_subJetIdx2[j]], subjet_eta[jet_subJetIdx2[j]], subjet_phi[jet_subJetIdx2[j]], subjet_pt[jet_subJetIdx2[j]]*(1.-subjet_rawFactor[jet_subJetIdx2[j]]), rho});
          } else {
            corrS1 = std::get<CompoundCorrection::Ref>(m_jesSFSubjet)->evaluate({subjet_eta[jet_subJetIdx1[j]], subjet_pt[jet_subJetIdx1[j]]*(1.-subjet_rawFactor[jet_subJetIdx1[j]])});
            corrS2 = std::get<CompoundCorrection::Ref>(m_jesSFSubjet)->evaluate({subjet_eta[jet_subJetIdx2[j]], subjet_pt[jet_subJetIdx2[j]]*(1.-subjet_rawFactor[jet_subJetIdx2[j]])});
          }
        }

        if( corrS1 > 0.){
          const double newcS1 = (1.-subjet_rawFactor[jet_subJetIdx1[j]])*corrS1;
          pt_sj_nom[jet_subJetIdx1[j]] *= newcS1;
          mass_sj_nom[jet_subJetIdx1[j]] *= newcS1;
        }

        if( corrS2 > 0.){
          const double newcS2 = (1.-subjet_rawFactor[jet_subJetIdx2[j]])*corrS2;
          pt_sj_nom[jet_subJetIdx2[j]] *= newcS2;
          mass_sj_nom[jet_subJetIdx2[j]] *= newcS2;
        }
        jet_groomedP4[j] = (
            LVectorM(pt_sj_nom[jet_subJetIdx1[j]], subjet_eta[jet_subJetIdx1[j]], subjet_phi[jet_subJetIdx1[j]], mass_sj_nom[jet_subJetIdx1[j]])
          + LVectorM(pt_sj_nom[jet_subJetIdx2[j]], subjet_eta[jet_subJetIdx2[j]], subjet_phi[jet_subJetIdx2[j]], mass_sj_nom[jet_subJetIdx2[j]]));
          msd_nom[j] = jet_groomedP4[j].M();
        if ( msd_nom[j] < 0.0) {
          msd_nom[j] *= -1.;
        }
      }
    }
  }

#ifdef BAMBOO_JME_DEBUG
  LogDebug << "JME:: Groomed momenta: ";
  for ( std::size_t i{0}; i != nJets; ++i ) {
    const auto& p4_g = jet_groomedP4[i];
    LogDebug << "(PT=" << p4_g.Pt() << ", ETA=" << p4_g.Eta() << ", PHI=" << p4_g.Phi() << ") ";
    if ( m_doJECSubjet ) {
      LogDebug <<"(mSD=" <<msd_nom[i]<< ") ";
    }
  }
  LogDebug << std::endl;
#endif
  // smearing and JER
  std::size_t iVar = 1; // after nominal
  if ( m_doSmearing ) {
    LogDebug << "JME:: Smearing (seed=" << seed << ")" << std::endl;
    p4compv_t pt_jerUp(pt_nom.size(), 0.), mass_jerUp(mass_nom.size(), 0.);
    p4compv_t pt_jerDown(pt_nom.size(), 0.), mass_jerDown(mass_nom.size(), 0.);
    for ( std::size_t i{0}; i != nJets; ++i ) {
      const auto eOrig = ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>>(pt_nom[i], jet_eta[i], jet_phi[i], mass_nom[i]).E();
      double smearFactor_nom{1.}, smearFactor_down{1.}, smearFactor_up{1.};
      if ( pt_nom[i] > 0. ) {
        const auto ptRes  = m_jetPtRes->evaluate({jet_eta[i], pt_nom[i], rho});
        LogDebug << "JME:: JetParameters: pt=" << pt_nom[i] << ", eta=" << jet_eta[i] << ", rho=" << rho << "; ptRes=" << ptRes << std::endl;
        LogDebug << "JME:: ";
        float genPt = -1;
        if ( m_smearDoGenMatch ) {
          const auto iGen = findGenMatch(pt_nom[i], jet_eta[i], jet_phi[i], jet_genJetIdx[i], genjet_pt, genjet_eta, genjet_phi, ptRes*pt_nom[i]);
          if ( iGen != genjet_pt.size() ) {
            genPt = genjet_pt[iGen];
            LogDebug << "genPt=" << genPt << " ";
          }
        }
        if ( m_jersfInputEtaPt ) {
          smearFactor_nom  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], pt_nom[i], "nom"})});
          smearFactor_up  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], pt_nom[i], "up"})});
          smearFactor_down  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], pt_nom[i], "down"})});         
        } else {
      	  smearFactor_nom  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], "nom"})});
          smearFactor_up  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], "up"})});
          smearFactor_down  = m_jerSmear->evaluate({pt_nom[i], jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], "down"})});
        }
        LogDebug << "JME:: SmearFactors are: NOMINAL=" << smearFactor_nom << ", DOWN=" << smearFactor_down << ", UP=" << smearFactor_up << std::endl;
      }
      pt_jerDown[i]   = pt_nom[i]*smearFactor_down;
      mass_jerDown[i] = mass_nom[i]*smearFactor_down;
      pt_jerUp[i]     = pt_nom[i]*smearFactor_up;
      mass_jerUp[i]   = mass_nom[i]*smearFactor_up;
      pt_nom[i]       *= smearFactor_nom;
      mass_nom[i]     *= smearFactor_nom;
    }
    if ( m_splitJER ) {
      ROOT::VecOps::RVec<int> jerBin(pt_nom.size(), -1);
      for ( std::size_t j{0}; j != nJets; ++j ) {
        jerBin[j] = jerSplitID(pt_nom[j], jet_eta[j]);
      }
      for ( int i{0}; i != 6; ++i ) {
        p4compv_t pt_jeriUp{pt_nom}, mass_jeriUp{mass_nom};
        p4compv_t pt_jeriDown{pt_nom}, mass_jeriDown{mass_nom};
        for ( std::size_t j{0}; j != nJets; ++j ) {
          if ( jerBin[j] == i ) {
            pt_jeriUp[j] = pt_jerUp[j];
            pt_jeriDown[j] = pt_jerDown[j];
            mass_jeriUp[j] = mass_jerUp[j];
            mass_jeriDown[j] = mass_jerDown[j];
          }
        }
        out.set(iVar++, std::move(pt_jeriUp)  , std::move(mass_jeriUp), std::move(msd_nom));
        out.set(iVar++, std::move(pt_jeriDown), std::move(mass_jeriDown), std::move(msd_nom));
      }
    } else {
      out.set(iVar++, std::move(pt_jerUp)  , std::move(mass_jerUp), std::move(msd_nom));
      out.set(iVar++, std::move(pt_jerDown), std::move(mass_jerDown), std::move(msd_nom));
    }
    LogDebug << "JME:: Done with smearing" << std::endl;
  } else {
    LogDebug << "JME:: No smearing" << std::endl;
  }

  // Nominal = first entry in result
  out.set(0, pt_nom, mass_nom, msd_nom);

  // HEM issue 2018, see https://hypernews.cern.ch/HyperNews/CMS/get/JetMET/2000.html
  if ( m_addHEM2018Issue ) {
    p4compv_t pt_down(pt_nom.size(), 0.), mass_down(mass_nom.size(), 0.), msd_down(msd_nom.size(), 0.);
    auto jet_groomedP4_HEM = std::vector<LVectorM>{nJets};
    for ( std::size_t j{0}; j != nJets; ++j ) {
      const auto delta = deltaHEM2018Issue(pt_nom[j], jet_jetId[j], jet_phi[j], jet_eta[j]);
      pt_down[j] = pt_nom[j]*delta;
      mass_down[j] = mass_nom[j]*delta;
      if ( m_doJECSubjet ) {
        if ( jet_subJetIdx1[j] >= 0 && jet_subJetIdx2[j] >= 0 ) {
          const auto delta1 = deltaHEM2018Issue(pt_sj_nom[jet_subJetIdx1[j]], jet_jetId[j], subjet_phi[jet_subJetIdx1[j]], subjet_eta[jet_subJetIdx1[j]]);
          const auto delta2 = deltaHEM2018Issue(pt_sj_nom[jet_subJetIdx2[j]], jet_jetId[j], subjet_phi[jet_subJetIdx2[j]], subjet_eta[jet_subJetIdx2[j]]);
          jet_groomedP4_HEM[j] = (
              LVectorM(delta1*pt_sj_nom[jet_subJetIdx1[j]], subjet_eta[jet_subJetIdx1[j]], subjet_phi[jet_subJetIdx1[j]], delta1*mass_sj_nom[jet_subJetIdx1[j]])
            + LVectorM(delta2*pt_sj_nom[jet_subJetIdx2[j]], subjet_eta[jet_subJetIdx2[j]], subjet_phi[jet_subJetIdx2[j]], delta2*mass_sj_nom[jet_subJetIdx2[j]]));
          msd_down[j] = jet_groomedP4_HEM[j].M();
          if ( msd_down[j] < 0.0 ) {
            msd_down[j] *= -1.;
          }
        }
      }
    }
    if ( m_doJECSubjet ) {
      out.set(iVar++, pt_nom, mass_nom, msd_nom);
      out.set(iVar++, std::move(pt_down), std::move(mass_down), std::move(msd_down));
    } else {
      out.set(iVar++, pt_nom, mass_nom, msd_nom);
      out.set(iVar++, std::move(pt_down), std::move(mass_down), msd_nom);
    }
  }
  // JES uncertainties
  for ( auto& jesUnc : m_jesUncSourcesSubjet ) {
    LogDebug << "JME:: evaluating JES uncertainty: " << jesUnc.first << std::endl;
    p4compv_t pt_jesDown(pt_nom.size(), 0.), mass_jesDown(mass_nom.size(), 0.), msd_jesDown(msd_nom.size(), 0.);
    p4compv_t pt_jesUp(pt_nom.size(), 0.), mass_jesUp(mass_nom.size(), 0.), msd_jesUp(msd_nom.size(), 0.);
    auto jet_groomedP4_jesDown = std::vector<LVectorM>{nJets};
    auto jet_groomedP4_jesUp = std::vector<LVectorM>{nJets};
    for ( std::size_t i{0}; i != nJets; ++i ) {
      const auto delta = jesUnc.second->evaluate({jet_eta[i], pt_nom[i]});
      LogDebug << "JME:: jet " << i << ", delta = " << delta << std::endl;
      pt_jesDown[i]   = pt_nom[i]*(1.-delta);
      mass_jesDown[i] = mass_nom[i]*(1.-delta);
      pt_jesUp[i]     = pt_nom[i]*(1.+delta);
      mass_jesUp[i]   = mass_nom[i]*(1.+delta);
      if ( m_doJECSubjet ) {
        if ( jet_subJetIdx1[i] >= 0 && jet_subJetIdx2[i] >= 0 ) {
          const auto delta1 = jesUnc.second->evaluate({subjet_eta[jet_subJetIdx1[i]], pt_sj_nom[jet_subJetIdx1[i]]});
          const auto delta2 = jesUnc.second->evaluate({subjet_eta[jet_subJetIdx2[i]], pt_sj_nom[jet_subJetIdx2[i]]});
          jet_groomedP4_jesDown[i] = (
              LVectorM((1.-delta1)*pt_sj_nom[jet_subJetIdx1[i]], subjet_eta[jet_subJetIdx1[i]], subjet_phi[jet_subJetIdx1[i]], (1.-delta1)*mass_sj_nom[jet_subJetIdx1[i]])
            + LVectorM((1.-delta2)*pt_sj_nom[jet_subJetIdx2[i]], subjet_eta[jet_subJetIdx2[i]], subjet_phi[jet_subJetIdx2[i]], (1.-delta2)*mass_sj_nom[jet_subJetIdx2[i]]));
          msd_jesDown[i] = jet_groomedP4_jesDown[i].M();
          if ( msd_jesDown[i] < 0.0 ) {
            msd_jesDown[i] *= -1.;
          }
          jet_groomedP4_jesUp[i] = (
              LVectorM((1.+delta1)*pt_sj_nom[jet_subJetIdx1[i]], subjet_eta[jet_subJetIdx1[i]], subjet_phi[jet_subJetIdx1[i]], (1.+delta1)*mass_sj_nom[jet_subJetIdx1[i]])
            + LVectorM((1.+delta2)*pt_sj_nom[jet_subJetIdx2[i]], subjet_eta[jet_subJetIdx2[i]], subjet_phi[jet_subJetIdx2[i]], (1.+delta2)*mass_sj_nom[jet_subJetIdx2[i]]));
          msd_jesUp[i] = jet_groomedP4_jesUp[i].M();
          if ( msd_jesUp[i] < 0.0 ) {
            msd_jesUp[i] *= -1.;
          }
        }
      }
    }
    if ( m_doJECSubjet ) {
      out.set(iVar++, std::move(pt_jesUp)  , std::move(mass_jesUp)  , std::move(msd_jesUp));
      out.set(iVar++, std::move(pt_jesDown), std::move(mass_jesDown), std::move(msd_jesDown));
    } else {
      out.set(iVar++, std::move(pt_jesUp)  , std::move(mass_jesUp), msd_nom);
      out.set(iVar++, std::move(pt_jesDown), std::move(mass_jesDown), msd_nom);
    }
  }

#ifdef BAMBOO_JME_DEBUG
  assert(iVar == out.size());
  LogDebug << "JME:: returning " << out.size() << " modified jet collections" << std::endl;
  const auto varNames = available();
  assert(varNames.size() == nVariations);
  for ( std::size_t i{0}; i != nVariations; ++i ) {
    LogDebug << "JME:: Jet_" << varNames[i] << ": ";
    for ( std::size_t j{0}; j != nJets; ++j ) {
      LogDebug << "(PT=" << out.pt(i)[j] << ", ETA=" << jet_eta[j] << ", PHI=" << jet_phi[j] << ", M=" << out.mass(i)[j] << ") ";
      if ( m_doJECSubjet ) {
        LogDebug << "(mSD=" << out.msoftdrop(i)[j] <<") ";
      }
    }
    LogDebug << std::endl;
  }
#endif
  return out;
}


std::vector<std::string> FatJetVariationsCalculator::available(const std::string&) const
{
  std::vector<std::string> products = { "nominal" };
  if ( m_doSmearing ) {
    if ( m_splitJER ) {
      for ( int i = 0; i != 6; ++i ) {
        products.emplace_back("jer"+std::to_string(i)+"up");
        products.emplace_back("jer"+std::to_string(i)+"down");
      }
    } else {
      products.emplace_back("jerup");
      products.emplace_back("jerdown");
    }
  }
  if ( m_addHEM2018Issue ) {
    products.emplace_back("jesHEMIssueup");
    products.emplace_back("jesHEMIssuedown");
  }
  for ( const auto& src : m_jesUncSources ) {
    products.emplace_back("jes"+src.first+"up");
    products.emplace_back("jes"+src.first+"down");
  }
  return products;
}

Type1METVariationsCalculator Type1METVariationsCalculator::create(
  const std::string& jsonFile,
  const std::string& jetAlgo,
  const std::string& jecTag,
  const std::string& jecLevel,
  const std::string& l1jec,
  float unclEnThreshold,
  float emEnFracThreshold,
  const std::vector<std::string>& jesUncertainties,
  bool addHEM2018Issue,
  bool isT1SmearedMET,
  const std::string& jerTag, const std::string& jsonFileSmearingTool,
  const std::string& smearingToolName, bool splitJER,
  bool doGenMatch, float genMatch_maxDR, float genMatch_maxDPT)
{
 Type1METVariationsCalculator inst{};
 auto cset = CorrectionSet::from_file(jsonFile);
 std::unique_ptr<correction::CorrectionSet> csetJerSmear = nullptr;
 if(!jsonFileSmearingTool.empty())
   csetJerSmear = CorrectionSet::from_file(jsonFileSmearingTool);
 configureMETCalc_common(inst, cset, csetJerSmear, jetAlgo, jecTag, jecLevel, l1jec,
  unclEnThreshold, emEnFracThreshold, jesUncertainties, isT1SmearedMET,
  jerTag, splitJER, smearingToolName, doGenMatch, genMatch_maxDR, genMatch_maxDPT);
 inst.setAddHEM2018Issue(addHEM2018Issue);
 inst.setUnclusteredEnergyTreshold(unclEnThreshold);
 inst.setEmEnergyFracThreshold(emEnFracThreshold);
 inst.setIsT1SmearedMET(isT1SmearedMET);
 return inst;
}

Type1METVariationsCalculator::result_t Type1METVariationsCalculator::produce(
   const p4compv_t& jet_pt, const p4compv_t& jet_eta, const p4compv_t& jet_phi, const p4compv_t& jet_mass,
   const p4compv_t& jet_rawcorr, const p4compv_t& jet_area,
   const p4compv_t& jet_muonSubtrFactor, const p4compv_t& jet_neEmEF, const p4compv_t& jet_chEmEF, const ROOT::VecOps::RVec<int>& jet_jetId,
   const float rho,
   const ROOT::VecOps::RVec<int>& jet_genJetIdx,
   const ROOT::VecOps::RVec<int>& jet_partonFlavour,
   const int seed,
   const p4compv_t& genjet_pt, const p4compv_t& genjet_eta, const p4compv_t& genjet_phi, const p4compv_t& genjet_mass,
   const float rawmet_phi, const float rawmet_pt,
   const p4compv_t& lowptjet_rawpt, const p4compv_t& lowptjet_eta, const p4compv_t& lowptjet_phi, const p4compv_t& lowptjet_area,
   const p4compv_t& lowptjet_muonSubtrFactor, const p4compv_t& lowptjet_neEmEF, const p4compv_t& lowptjet_chEmEF,
   const float met_unclustenupdx, const float met_unclustenupdy
   ) const
{
  const auto nJets = jet_pt.size();
  const auto nVariations = 3+( m_doSmearing ? 2*( m_splitJER ? 6 : 1 ) : 0 )+2*m_jesUncSources.size()+( m_addHEM2018Issue ? 2 : 0 ); // 1(nom)+2(unclust)+2(JER[*6])+2*len(JES)[+2(HEM)]
  result_t out{nVariations, rawmet_pt*std::cos(rawmet_phi), rawmet_pt*std::sin(rawmet_phi)};
  LogDebug << "JME:: hello from Type1METVariations produce. Got " << jet_pt.size() << " jets and " << lowptjet_rawpt.size() << " low-PT jets" << std::endl;
  LogDebug << "JME:: Smearing (seed=" << seed << ")" << std::endl;
  LogDebug << "JME:: RawMET pt: " << rawmet_pt << ", phi: " << rawmet_phi << std::endl;

  // normal jets
  addVariations(out, jet_pt, jet_eta, jet_phi, jet_mass,
      jet_rawcorr, jet_area, jet_muonSubtrFactor, jet_neEmEF, jet_chEmEF, jet_jetId,
		rho, jet_genJetIdx, jet_partonFlavour, genjet_pt, genjet_eta, genjet_phi, genjet_mass, seed);

  //low-PT jets
  p4compv_t lowptjet_zero(lowptjet_rawpt.size(), 0.);
  const ROOT::VecOps::RVec<int> lowptjet_zero_int(lowptjet_rawpt.size(), 0);
  const ROOT::VecOps::RVec<int> lowptjet_mOne_int(lowptjet_rawpt.size(), -1);
  addVariations(out, lowptjet_rawpt, lowptjet_eta, lowptjet_phi, lowptjet_zero,
      lowptjet_zero, lowptjet_area, lowptjet_muonSubtrFactor,
      ( lowptjet_neEmEF.empty() ? lowptjet_zero : lowptjet_neEmEF  ),
      ( lowptjet_chEmEF.empty() ? lowptjet_zero : lowptjet_chEmEF  ),
      lowptjet_zero_int, rho, lowptjet_zero_int, lowptjet_mOne_int,
		genjet_pt, genjet_eta, genjet_phi, genjet_mass, seed);

  // unclustered energy, base on nominal(0)
  out.setXY(nVariations-2, out.px(0)+met_unclustenupdx, out.py(0)+met_unclustenupdy);
  out.setXY(nVariations-1, out.px(0)-met_unclustenupdx, out.py(0)-met_unclustenupdy);  
#ifdef BAMBOO_JME_DEBUG
  LogDebug << "JME:: returning " << out.size() << " modified METs" << std::endl;
  const auto varNames = available();
  assert(varNames.size() == nVariations);
  for ( std::size_t i{0}; i != nVariations; ++i ) {
    LogDebug << "JME:: MET_" << varNames[i] << ": PT=" << out.pt(i) << ", PHI=" << out.phi(i) << std::endl;
  }
#endif
  return out;
}

// for a single jet collection
void Type1METVariationsCalculator::addVariations(Type1METVariationsCalculator::result_t& out,
    const p4compv_t& jet_pt, const p4compv_t& jet_eta, const p4compv_t& jet_phi, const p4compv_t& jet_mass,
    const p4compv_t& jet_rawcorr, const p4compv_t& jet_area, const p4compv_t& jet_muonSubtrFactor,
    const p4compv_t& jet_neEmEF, const p4compv_t& jet_chEmEF, const ROOT::VecOps::RVec<int>& jet_jetId, const float rho,
    const ROOT::VecOps::RVec<int>& jet_genJetIdx, const ROOT::VecOps::RVec<int>& jet_partonFlavour, 
    const p4compv_t& genjet_pt, const p4compv_t& genjet_eta, const p4compv_t& genjet_phi, const p4compv_t& genjet_mass,
		const int seed
) const
{
  const auto nJets = jet_pt.size();
  for( std::size_t i{0}; i != nJets; ++i) {
    // L1 and full (L1L2L3Res) JEC for muon-subtracted jet
    float corr_L1L2L3Res;
    if (auto corrObj = std::get_if<Correction::Ref>(&m_jesSF)) {
      if (m_jecInputAreaRho)
        corr_L1L2L3Res = (*corrObj)->evaluate({jet_area[i], jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
      else if (m_jecInputAreaRhoPhi)
        corr_L1L2L3Res = (*corrObj)->evaluate({jet_area[i], jet_eta[i], jet_phi[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
      else
        corr_L1L2L3Res = (*corrObj)->evaluate({jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i])});
    } else {
      if (m_jecInputAreaRho)
        corr_L1L2L3Res = std::get<CompoundCorrection::Ref>(m_jesSF)->evaluate({jet_area[i], jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
      else if (m_jecInputAreaRhoPhi)
        corr_L1L2L3Res = std::get<CompoundCorrection::Ref>(m_jesSF)->evaluate({jet_area[i], jet_eta[i], jet_phi[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
      else
        corr_L1L2L3Res = std::get<CompoundCorrection::Ref>(m_jesSF)->evaluate({jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i])});
    }
    float corr_L1 = 1;
    if ( m_doL1JEC ){
      corr_L1 = m_jetLevel1->evaluate({jet_area[i],jet_eta[i], jet_pt[i]*(1.-jet_rawcorr[i]), rho});
    }
    if (corr_L1L2L3Res <= 0.){corr_L1L2L3Res = 1.;}
    if (corr_L1 <= 0.){corr_L1 = 1.;}

    const auto jet_pt_raw = jet_pt[i]*(1-jet_rawcorr[i]);
    const double jet_pt_raw_nomu = jet_pt_raw*(1-jet_muonSubtrFactor[i]);
    const double muon_pt = jet_pt_raw*jet_muonSubtrFactor[i];
    const auto jet_pt_nomuL1L2L3 = jet_pt_raw_nomu*corr_L1L2L3Res;
    const auto jet_pt_nomuL1     = jet_pt_raw_nomu*corr_L1;
    const auto jet_pt_L1L2L3 = jet_pt_nomuL1L2L3 + muon_pt;
    const auto jet_pt_L1     = jet_pt_nomuL1     + muon_pt;
    const auto jet_mass_L1L2L3 = jet_mass[i]*(1-jet_rawcorr[i])*corr_L1L2L3Res;
    LogDebug << "JME:: jet_muonSubtrFactor[i]: " << jet_muonSubtrFactor[i] << std::endl; 
    LogDebug << "JME:: jecL1L2L3=" << corr_L1L2L3Res << ", jecL1=" << corr_L1 << "; PT_L1L2L3=" << jet_pt_L1L2L3 << ", PT_L1=" << jet_pt_L1 << ", PT_mu=" << muon_pt << std::endl;
    // smearing and JER
    double smearFactor_nom{1.}, smearFactor_down{1.}, smearFactor_up{1.};
    if ( m_doSmearing ) {
      float genPt = -1;
      const auto eOrig = ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>>(jet_pt_L1L2L3, jet_eta[i], jet_phi[i], jet_mass_L1L2L3).E();
      if (jet_pt_L1L2L3 > 0.){
        const auto ptRes  = m_jetPtRes->evaluate({jet_eta[i], jet_pt_L1L2L3, rho});
        LogDebug << "JME:: JetParameters: pt=" << jet_pt_L1L2L3 << ", eta=" << jet_eta[i] << ", rho=" << rho << "; ptRes=" << ptRes << std::endl;
        LogDebug << "JME:: ";
        if ( m_smearDoGenMatch ) {
          const auto iGen = findGenMatch(jet_pt_L1L2L3, jet_eta[i], jet_phi[i], jet_genJetIdx[i], genjet_pt, genjet_eta, genjet_phi, ptRes*jet_pt_L1L2L3);
          if ( iGen != genjet_pt.size() ) {
            genPt = genjet_pt[iGen];
            LogDebug << "genPt=" << genPt << " ";
          }
        }
        if ( m_jersfInputEtaPt ) {
          smearFactor_nom  = m_jerSmear->evaluate({jet_pt_L1L2L3, jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], jet_pt_L1L2L3, "nom"})});
          smearFactor_up  = m_jerSmear->evaluate({jet_pt_L1L2L3, jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], jet_pt_L1L2L3, "up"})});
          smearFactor_down  = m_jerSmear->evaluate({jet_pt_L1L2L3, jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], jet_pt_L1L2L3, "down"})});          
        } else {
    	    smearFactor_nom  = m_jerSmear->evaluate({jet_pt_L1L2L3, jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], "nom"})});
          smearFactor_up  = m_jerSmear->evaluate({jet_pt_L1L2L3, jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], "up"})});
          smearFactor_down  = m_jerSmear->evaluate({jet_pt_L1L2L3, jet_eta[i], genPt, rho, seed, ptRes, m_jetEResSF->evaluate({jet_eta[i], "down"})});
        }
      }
    }
    LogDebug << "JME:: SmearFactor are: NOMINAL " << smearFactor_nom << ", UP: " << smearFactor_up << ", DOWN: "<< smearFactor_down << std::endl;
    if ( ( jet_pt_nomuL1L2L3 > m_unclEnThreshold ) && ( (jet_neEmEF[i]+jet_chEmEF[i]) < m_emEnFracThreshold ) ) {
      std::size_t iVar = 0;
      const auto jet_cosPhi = std::cos(jet_phi[i]);
      const auto jet_sinPhi = std::sin(jet_phi[i]);
      if ( ! ( m_doSmearing && m_isT1SmearedMET) ) {
        out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, jet_pt_L1 - jet_pt_L1L2L3);             // nominal
      }
      auto jet_pt_L1p = jet_pt_L1; // with optional offset for JES uncertainty calculation if nominal is smeared
      if ( m_doSmearing ) {
        const auto dr_jernom = jet_pt_L1 - jet_pt_L1L2L3 * smearFactor_nom;
        if ( m_isT1SmearedMET ) {
          const auto dr_jerup   = jet_pt_L1 - jet_pt_L1L2L3*smearFactor_up;
          const auto dr_jerdown = jet_pt_L1 - jet_pt_L1L2L3*smearFactor_down;
          out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, dr_jernom);                         // smeared nominal
          if ( m_splitJER ) {
            const auto jerBin = jerSplitID(jet_pt_L1L2L3 * smearFactor_nom, jet_eta[i]);
            for ( int k{0}; k != 6; ++k ) {
              if ( jerBin == k ) { // vary
                out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, dr_jerup);                    // JER[k]-up
                out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, dr_jerdown);                  // JER[k]-down
              } else { // keep nominal
                out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, dr_jernom);                   // JER[k]-up
                out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, dr_jernom);                   // JER[k]-down
              }
            }
          }
          else{
            out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, dr_jerup);                        // JER-up
            out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, dr_jerdown);                      // JER-down
          }
          jet_pt_L1p += jet_pt_L1L2L3*(1.-smearFactor_nom); // offset for JES uncertainties, since the nominal is smeared
        } else{
          for ( std::size_t k{0}; k != ( m_splitJER ? 6 : 1 ); ++k ) {
            out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, dr_jernom);                     // JER[k]-up
            out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, dr_jernom);                     // JER[k]-down
          }
        }
      }
      if ( m_addHEM2018Issue ) {
        const auto delta = deltaHEM2018Issue(jet_pt_L1L2L3 * smearFactor_nom, jet_jetId[i], jet_phi[i], jet_eta[i]);
        out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, jet_pt_L1p - jet_pt_L1L2L3);           // up = nominal
        out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, jet_pt_L1p - jet_pt_L1L2L3*delta);     // down
      }
      // JES uncertainties
      for ( auto& jesUnc : m_jesUncSources ) {
        LogDebug << "JME:: evaluating JES uncertainty: " << jesUnc.first << std::endl;
        float delta = 0.;
        const auto partonFlav = std::abs(jet_partonFlavour[i]);
        if (!(jesUnc.first == "FlavorPureGluon" && partonFlav != 21) &&
          !(jesUnc.first == "FlavorPureQuark" && !(partonFlav >= 1 && partonFlav <= 3)) &&
          !(jesUnc.first == "FlavorPureCharm" && partonFlav != 4) &&
          !(jesUnc.first == "FlavorPureBottom" && partonFlav != 5)) {
          delta = jesUnc.second->evaluate({jet_eta[i], jet_pt_L1L2L3});
        }
        LogDebug << " JES uncertainty " << jesUnc.first << " : " << delta << std::endl;
        out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, jet_pt_L1p - jet_pt_L1L2L3*(1+delta)); // JES_i-up
        out.addR_proj(iVar++, jet_cosPhi, jet_sinPhi, jet_pt_L1p - jet_pt_L1L2L3*(1-delta)); // JES_i-down
      }
    }
  }
}


std::vector<std::string> Type1METVariationsCalculator::available(const std::string&) const
{
 if ((!m_doJEC) || (!m_doL1JEC)) {
   throw std::runtime_error("The calculator is not fully configured (for MET variations both setJEC and setL1JEC need to be called)");
 }
 std::vector<std::string> products = { "nominal" };
 if ( m_doSmearing ) {
   if ( m_splitJER ) {
     for ( int i = 0; i != 6; ++i ) {
       products.emplace_back("jer"+std::to_string(i)+"up");
       products.emplace_back("jer"+std::to_string(i)+"down");
     }
   } else {
     products.emplace_back("jerup");
     products.emplace_back("jerdown");
   }
 }
 if ( m_addHEM2018Issue ) {
   products.emplace_back("jesHEMIssueup");
   products.emplace_back("jesHEMIssuedown");
 }
 for ( const auto& src : m_jesUncSources ) {
   products.emplace_back("jes"+src.first+"up");
   products.emplace_back("jes"+src.first+"down");
 }
 products.emplace_back("unclustEnup");
 products.emplace_back("unclustEndown");
 return products;
}
